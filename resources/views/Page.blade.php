  <!DOCTYPE html>
  <html lang="en">

  <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <title>Our Portfolio</title>
      <meta name="robots" content="noindex, nofollow">
      <meta content="" name="description">
      <meta content="" name="keywords">
      <link href="{{asset('images/favicon.png')}}" rel="icon">
      <link href="{{asset('images/apple-touch-icon.png')}}" rel="apple-touch-icon">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
      <link href="{{asset('css/aos.css')}}" rel="stylesheet">
      <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{asset('css/bootstrap-icons.css')}}" rel="stylesheet">
      <link href="{{asset('css/boxicons.min.css')}}" rel="stylesheet">
      <link href="{{asset('css/glightbox.min.css')}}" rel="stylesheet">
      <link href="{{asset('css/remixicon.css')}}" rel="stylesheet">
      <link href="{{asset('css/swiper-bundle.min.css')}}" rel="stylesheet">
      <link href="{{asset('css/style.css')}}" rel="stylesheet">
  </head>

  <body>

  <a href="tel:0014034797775" style="cursor: pointer;" class="float-phone" target="_blank">
        <i class="fa fa-phone my-float"></i>
    </a>


      <a href="https://api.whatsapp.com/send?phone=0014034797775" style="cursor: pointer;" class="float-watsapp" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>


      <!-- ======= Header ======= -->
      <header id="header" class="fixed-top ">
          <div class="container d-flex align-items-center">

              <h1 class="me-auto" >
                  <img style="padding-left: 3px" src="images\logo.png" height="70" alt="Logo">
              </h1>


              <nav id="navbar" class="navbar">
                  <ul>
                      <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
                      <li><a class="nav-link scrollto" href="#about">About</a></li>
                      <li><a class="nav-link scrollto" href="#services">Services</a></li>
                      <li><a class="nav-link   scrollto" href="#portfolio">Portfolio</a></li>
                      <li><a class="nav-link scrollto" href="#team">Team</a></li>
                      <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
                      <li><a class="getstarted scrollto" href="#contact">Hire Us!</a></li>
                  </ul>
                  <i class="bi bi-list mobile-nav-toggle"></i>
              </nav><!-- .navbar -->

          </div>
      </header><!-- End Header -->

      <!-- ======= Hero Section ======= -->
      <section id="hero" class="d-flex align-items-center">

          <div class="container">
              <div class="row">
                  <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1"
                      data-aos="fade-up" data-aos-delay="200">
                      <h1>Best Web-Development Services For You.</h1>
                      <h2>We are team of talented Developer making websites of your prefrence!</h2>
                      <div class="d-flex justify-content-center justify-content-lg-start">
                          <a href="#contact" class="btn-get-started scrollto">Get Started</a>
                      </div>
                  </div>
                  <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
                      <img src="images/hero-img.png" class="img-fluid animated" alt="">
                  </div>
              </div>
          </div>

      </section><!-- End Hero -->

      <main id="main">
          <!-- ======= About Us Section ======= -->
          <section id="about" class="about">
              <div class="container" data-aos="fade-up">

                  <div class="section-title">
                      <h2>About Us</h2>
                  </div>

                  <div class="row content">
                      <div class="col-lg-6">
                          <p>
                              Welcome to <b>Essential Technology</b>, your trusted partner in web development solutions.
                              We are dedicated to crafting innovative and user-centric websites that elevate your online
                              presence and drive results.
                          </p>
                          <ul>
                              <li><i class="ri-check-double-line"></i> Customized Solutions
                              </li>
                              <li><i class="ri-check-double-line"></i> Cutting-Edge Technology and Frameworks
                              </li>
                              <li><i class="ri-check-double-line"></i> Transparent Communication
                              </li>
                          </ul>
                      </div>
                      <div class="col-lg-6 pt-4 pt-lg-0">
                          <p>
                              Web prioritize three core principles in our web development process: speed, security, and
                              quality. <br> With a focus on efficiency and utilizing the latest security protocols, we
                              ensure that your website is not only delivered promptly but also fortified against online
                              threats. <br> Our commitment to excellence guarantees that your website not only functions
                              flawlessly but also leaves a lasting impression on your visitors.
                          </p>
                          <a href="#contact" class="btn-learn-more">Learn More</a>
                      </div>
                  </div>

              </div>
          </section><!-- End About Us Section -->

          <!-- ======= Why Us Section ======= -->
          <section id="why-us" class="why-us section-bg">
              <div class="container-fluid" data-aos="fade-up">

                  <div class="row">

                      <div
                          class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

                          <div class="content">
                              <h3>What we <strong>get asked daily!</strong></h3>
                              <p>
                                  Your Questions and problems are the things that matter to us!
                              </p>
                          </div>

                          <div class="accordion-list">
                              <ul>
                                  <li>
                                      <a data-bs-toggle="collapse" class="collapse"
                                          data-bs-target="#accordion-list-1"><span>01</span> How long does it take to
                                          complete a website project? <i class="bx bx-chevron-down icon-show"></i><i
                                              class="bx bx-chevron-up icon-close"></i></a>
                                      <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                                          <p>
                                              The timeline for a website project can vary depending on its complexity
                                              and requirements. Generally, we aim to deliver projects within the given,
                                              ensuring quality and efficiency every step of the way.
                                          </p>
                                      </div>
                                  </li>

                                  <li>
                                      <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2"
                                          class="collapsed"><span>02</span> What is included in your web design
                                          services?
                                          <i class="bx bx-chevron-down icon-show"></i><i
                                              class="bx bx-chevron-up icon-close"></i></a>
                                      <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                                          <p>
                                              Our web design services encompass everything from initial concept creation
                                              to the final implementation. We focus on creating visually appealing and
                                              user-friendly websites that align with your brand identity and objectives.
                                          </p>
                                      </div>
                                  </li>

                                  <li>
                                      <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3"
                                          class="collapsed"><span>03</span> What sets your web development services
                                          apart from others?
                                          <i class="bx bx-chevron-down icon-show"></i><i
                                              class="bx bx-chevron-up icon-close"></i></a>
                                      <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                                          <p>
                                              Our web development services prioritize functionality, security, and
                                              scalability. We leverage the latest technologies and coding standards to
                                              create robust and future-proof websites that meet your business goals
                                              effectively. </p>
                                      </div>
                                  </li>

                              </ul>
                          </div>

                      </div>

                      <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img"
                          style="background-image: url(&quot;images/why-us.png&quot;);" data-aos="zoom-in"
                          data-aos-delay="150">&nbsp;</div>
                  </div>

              </div>
          </section><!-- End Why Us Section -->

          <!-- ======= Skills Section ======= -->
          <section id="skills" class="skills">
              <div class="container" data-aos="fade-up">

                  <div class="row">
                      <div class="col-lg-6 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
                          <img src="images/skills.png" class="img-fluid" alt="">
                      </div>
                      <div class="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
                          <h3>Our Expertise that we would offer in the making of your Website!</h3>
                          <p class="fst-italic">
                              Trust our expertise in web development, design, and SEO enhancement for streamlined and
                              impactful digital solutions.
                          </p>

                          <div class="skills-content">

                              <div class="progress">
                                  <span style="color:red" class="skill">Laravel<i class="val"></i></span>
                                  <div class="progress-bar-wrap">
                                      <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0"
                                          aria-valuemax="100"></div>
                                  </div>
                              </div>

                              <div class="progress">
                                  <span style="color:green" class="skill">Django<i class="val"></i></span>
                                  <div class="progress-bar-wrap">
                                      <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0"
                                          aria-valuemax="100"></div>
                                  </div>
                              </div>

                              <div class="progress">
                                  <span style="color:darkseagreen" class="skill">Node Js<i class="val"></i></span>
                                  <div class="progress-bar-wrap">
                                      <div class="progress-bar" role="progressbar" aria-valuenow="82" aria-valuemin="0"
                                          aria-valuemax="100"></div>
                                  </div>
                              </div>

                              <div class="progress">
                                  <span style="color:orange" class="skill">HTML<i class="val"></i></span>
                                  <div class="progress-bar-wrap">
                                      <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0"
                                          aria-valuemax="100"></div>
                                  </div>
                              </div>

                              <div class="progress">
                                  <span style="color:blue" class="skill">CSS<i class="val"></i></span>
                                  <div class="progress-bar-wrap">
                                      <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0"
                                          aria-valuemax="100"></div>
                                  </div>
                              </div>
                              <div class="progress">
                                  <span style="color:purple" class="skill">Bootstrap<i class="val"></i></span>
                                  <div class="progress-bar-wrap">
                                      <div class="progress-bar" role="progressbar" aria-valuenow="98" aria-valuemin="0"
                                          aria-valuemax="100"></div>
                                  </div>
                              </div>

                          </div>

                      </div>
                  </div>

              </div>
          </section><!-- End Skills Section -->

          <!-- ======= Services Section ======= -->
          <section id="services" class="services section-bg">
              <div class="container" data-aos="fade-up">

                  <div class="section-title">
                      <h2>Services</h2>
                      <p>
                          We specialize in comprehensive web solutions, including cutting-edge web design, seamless web
                          development, and strategic web enhancement services. Our expertise extends to simple SEO
                          techniques, ensuring your online presence stands out effectively. Let us elevate your digital
                          footprint with our tailored services.</p>
                  </div>

                  <div class="row">
                      <div class="col-xl-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                          <div class="icon-box">
                              <div class="icon"><i class="bx bxl-dribbble"></i></div>
                              <h4><a href="">Web Design</a></h4>
                              <p>Looking for a website that converts? Our expert web design team creates experiences
                                  that wow and drive results.</p>
                              <br>
                              <p><b>This service is bundled up with Web Development.</b></p>
                          </div>
                      </div>

                      <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in"
                          data-aos-delay="200">
                          <div class="icon-box">
                              <div class="icon"><i class="bx bx-file"></i></div>
                              <h4><a href="">Web Development</a></h4>
                              <p>Powerful functionality meets user-friendly design. Our web development services ensure
                                  your website works flawlessly.</p>
                          </div>
                      </div>

                      <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in"
                          data-aos-delay="300">
                          <div class="icon-box">
                              <div class="icon"><i class="bx bx-layer"></i></div>
                              <h4><a href="">Web Ai</a></h4>
                              <p>AI-powered development for lightning-fast speed. Get your website up and running
                                  quicker than ever before.</p>
                              <br>
                              <br>
                              <p><b>This service is bundled up with Web Development.</b></p>
                          </div>
                      </div>

                      <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in"
                          data-aos-delay="400">
                          <div class="icon-box">

                              <div class="icon"><i class="bx bx-tachometer"></i></div>
                              <h4><a href="">SEO</a></h4>
                              <p>Skyrocket your website's ranking with our data-driven SEO. We optimize content, build
                                  authority, and drive organic traffic for sustainable growth.</p>
                          </div>
                      </div>

                  </div>

              </div>
          </section><!-- End Services Section -->

          <!-- ======= Cta Section ======= -->
          <section id="cta" class="cta">
              <div class="container" data-aos="zoom-in">

                  <div class="row">
                      <div class="col-lg-9 text-center text-lg-start">
                          <h3>Call To Action</h3>
                          <p> At our company, we prioritize responsiveness and understanding to ensure your needs are
                              met promptly and effectively. We're committed to answering your calls as quickly as
                              possible. Feel free to reach out to us at +1 (403) 479-7775. Additionally, we strive to
                              comprehend your requirements thoroughly, ensuring that we deliver solutions tailored to
                              your unique preferences. Your satisfaction is our utmost priority. Let's connect and
                              discuss how we can assist you today!</p>
                      </div>
                      <div class="col-lg-3 cta-btn-container text-center">
                          <a class="cta-btn align-middle" href="#">Call To Action</a>
                      </div>
                  </div>

              </div>
          </section><!-- End Cta Section -->

          <!-- ======= Portfolio Section ======= -->
          <section id="portfolio" class="portfolio">
              <div class="container" data-aos="fade-up">

                  <div class="section-title">
                      <h2>Portfolio</h2>
                      <p>Welcome to our portfolio! Take a glimpse into our past projects and see the quality of work we
                          deliver. <br> Below are some of the websites we've had the pleasure to work on:</p>
                  </div>



                  <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

                      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                      <div style="box-shadow: 0 0px 8px rgba(0,0,0,0.6);">
                          <a href="https://theexpresstaste.000webhostapp.com/">
                              <div class="portfolio-img">
                                  <img src="images/ExpressTaste.png" class="img-fluid" alt="">
                              </div>
                          </a>
                          <div class="portfolio-info">
                              <h4>ExpressTaste</h4>
                          </div>
                          </div>
                      </div>



                      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                      <div style="box-shadow: 0 0px 8px rgba(0,0,0,0.6);">
                          <a href="https://sunchoice.us ">
                              <div class="portfolio-img"><img src="images/Sunchoice.png" class="img-fluid" alt=""></div>
                          </a>
                          <div class="portfolio-info">
                              <h4>SunChoice</h4>

                          </div>
                          </div>
                      </div>


                      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                      <div style="box-shadow: 0 0px 8px rgba(0,0,0,0.6);">
                          <a href="http://kodextech.net/workitpt/">
                              <div class="portfolio-img"><img src="images/Workitpt.png" class="img-fluid" alt=""></div>
                          </a>
                          <div class="portfolio-info">
                              <h4>WorkITPT</h4>

                          </div>
                          </div>
                      </div>

                      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                      <div style="box-shadow: 0 0px 8px rgba(0,0,0,0.6);">
                          <a href="https://roomatoz.com/">
                              <div class="portfolio-img"><img src="images/Roomatoz.png" class="img-fluid" alt=""></div>
                          </a>
                          <div class="portfolio-info">
                              <h4>Room A to Z</h4>
                          </div>
                          </div>
                      </div>

                      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                      <div style="box-shadow: 0 0px 8px rgba(0,0,0,0.6);">
                          <a href="http://kodextech.net/starstudent/">
                              <div class="portfolio-img"><img src="images/starstudent.png" class="img-fluid" alt="">
                              </div>
                          </a>
                          <div class="portfolio-info">
                              <h4>Starstudent</h4>
                          </div>
                          </div>
                      </div>

                      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                      <div style="box-shadow: 0 0px 8px rgba(0,0,0,0.6);">
                          <a href="https://imadnoortechnical.com">
                              <div class="portfolio-img"><img src="images/Imadnoortechnical.png" class="img-fluid"
                                      alt=""></div>
                          </a>
                          <div class="portfolio-info">
                              <h4>ImadNoorTechnical</h4>
                          </div>
                          </div>
                      </div>



                  </div>

              </div>
          </section><!-- End Portfolio Section -->

          <!-- ======= Team Section ======= -->
          <section id="team" class="team section-bg">
              <div class="container" data-aos="fade-up">

                  <div class="section-title">
                      <h2>Team</h2>
                      <p>Our team comprises hard-working, talented, and experienced professionals who are dedicated to
                          providing you with the best work possible. With their expertise and commitment, rest assured
                          that your projects are in capable hands. We strive to exceed expectations and deliver
                          exceptional results tailored to your needs.</p>
                  </div>

                  <div class="row">

                      <div class="col-lg-6 mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="200">
                          <div class="member d-flex align-items-start">
                              <div class="pic"><img src="images/team-2.png" class="img-fluid" alt=""></div>
                              <div class="member-info">
                                  <h4>Tayyeb Gondal</h4>
                                  <span>Senior Web-Developer</span>
                                  <p>Our professional developer with 4 years of experience!</p>
                                  <div class="social">
                                      <a href=""><i class="ri-twitter-fill"></i></a>
                                      <a href=""><i class="ri-facebook-fill"></i></a>
                                      <a href=""><i class="ri-instagram-fill"></i></a>
                                      <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-lg-6" data-aos="zoom-in" data-aos-delay="100">
                          <div class="member d-flex align-items-start">
                              <div class="pic"><img src="images/team-1.png" class="img-fluid" alt=""></div>
                              <div class="member-info">
                                  <h4>M.Abdullah</h4>
                                  <span>Web Developer</span>
                                  <p>Prominent Member our web developer team!</p>
                                  <div class="social">
                                      <a href=""><i class="ri-twitter-fill"></i></a>
                                      <a href=""><i class="ri-facebook-fill"></i></a>
                                      <a href=""><i class="ri-instagram-fill"></i></a>
                                      <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                                  </div>
                              </div>
                          </div>
                      </div>



                      <div class="col-lg-6 mt-4" data-aos="zoom-in" data-aos-delay="300">
                          <div class="member d-flex align-items-start">
                              <div class="pic"><img src="images/team-3.png" class="img-fluid" alt=""></div>
                              <div class="member-info">
                                  <h4>Tahir Gondal</h4>
                                  <span>Software Engineer</span>
                                  <p>Prominent Member our web developer team!</p>
                                  <div class="social">
                                      <a href=""><i class="ri-twitter-fill"></i></a>
                                      <a href=""><i class="ri-facebook-fill"></i></a>
                                      <a href=""><i class="ri-instagram-fill"></i></a>
                                      <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-lg-6 mt-4" data-aos="zoom-in" data-aos-delay="300">
                          <div class="member d-flex align-items-start">
                              <div class="pic"><img src="images/team-4.png" class="img-fluid" alt=""></div>
                              <div class="member-info">
                                  <h4>Mutasif Gondal</h4>
                                  <span>Business Developer</span>
                                  <p>Our Business handling Companion</p>
                                  <div class="social">
                                      <a href=""><i class="ri-twitter-fill"></i></a>
                                      <a href=""><i class="ri-facebook-fill"></i></a>
                                      <a href=""><i class="ri-instagram-fill"></i></a>
                                      <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                                  </div>
                              </div>
                          </div>
                      </div>

                  </div>

              </div>
          </section><!-- End Team Section -->


          <!-- ======= Contact Section ======= -->
          <section id="contact" class="contact">
              <div class="container" data-aos="fade-up">

                  <div class="section-title">
                      <h2>Contact Us</h2>
                      <p>For inquiries or assistance, please feel free to contact us at: <span
                              style="color:lightblue"><b> your-email@example.com</b></span><br>
                          Phone: <span style="color:lightblue"><b>+1 (403) 479-7775</b></span> <br>
                  </div>

                  <div class="row">

                      <div class="col-lg-5 d-flex align-items-stretch">
                          <div class="info">

                              <div class="email">
                                  <i class="bi bi-envelope"></i>
                                  <h4>Email:</h4>
                                  <p><a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                          data-cfemail="a3cacdc5cce3c6dbc2ced3cfc68dc0ccce">[email&nbsp;protected]</a>
                                  </p>
                              </div>

                              <div class="phone">
                                  <i class="bi bi-phone"></i>
                                  <h4>Call/<span style="color:green">Watsapp</span>:</h4>
                                  <p>+1 (403) 479-7775</p>
                              </div>

                              <a
                                  href="https://www.google.com/maps/place/Calgary,+AB,+Canada/data=!4m2!3m1!1s0x537170039f843fd5:0x266d3bb1b652b63a?sa=X&ved=1t:242&ictx=111"><img
                                      src="images/Location.png" alt="" style="border-radius:10px"></a>
                          </div>

                      </div>

                      <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
                          <form onsubmit="sendEmail(); reset(); return false" action="forms/contact.php" method="post"
                              role="form" class="php-email-form">
                              <div class="row">
                                  <div class="form-group col-md-6">
                                      <input type="text" name="name" class="form-control" id="name"
                                          placeholder="Full Name" required="">
                                  </div>
                                  <div class="form-group col-md-6">

                                      <input type="email" class="form-control" name="email" id="email"
                                          placeholder="Email Address" required="">
                                  </div>
                              </div>

                              <div class="row">
                                  <div class="form-group col-md-6">
                                      <input type="text" name="name" class="form-control" id="name"
                                          placeholder="Phone Number" required="">
                                  </div>

                                  <div class="form-group col-md-6">
                                      <input type="text" name="name" class="form-control" id="name"
                                          placeholder="Subject" required="">
                                  </div>
                              </div>

                              <div class="form-group">
                                  <textarea class="form-control" name="message" rows="10" required=""
                                      placeholder="Your Requirements / Message"></textarea>
                              </div>
                              <div class="form-group">

                              </div>
                              <div class="my-3">
                                  <div class="loading">Loading...</div>
                                  <div class="error-message"></div>
                                  <div class="sent-message">Your message has been sent. Thank you!</div>
                              </div>
                              <div class="text-center"><button type="submit">Send Message</button></div>
                          </form>
                      </div>

                  </div>

              </div>
          </section><!-- End Contact Section -->

      </main><!-- End #main -->

      <!-- ======= Footer ======= -->
      <footer id="footer">

          <div class="footer-top">
              <div class="container">
                  <div class="row">

                      <div class="col-lg-3 col-md-6 footer-contact">
                          <h1 class="me-auto" style="background-color:whitesmoke;padding:5px;border-radius:10px">
                              <img style="padding-left: 3px" src="images\logo.png" height="70" alt="Logo">
                              <img src="images\name.png" height="50" alt="Logo">
                          </h1>
                          <p>
                              <strong>Location:</strong>Calgary , Alberta, Canada<br>
                              <strong>Phone:</strong> +1 (403) 479-7775<br>
                              <strong>Email:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                  data-cfemail="83eaede5ecc3e6fbe2eef3efe6ade0ecee">[email&nbsp;protected]</a><br>
                          </p>
                      </div>

                      <div class="col-lg-3 col-md-6 footer-links">
                          <h4>Useful Links</h4>
                          <ul>
                              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                              <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                          </ul>
                      </div>

                      <div class="col-lg-3 col-md-6 footer-links">
                          <h4>Our Services</h4>
                          <ul>
                              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
                              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Enhancement</a></li>

                          </ul>
                      </div>

                      <div class="col-lg-3 col-md-6 footer-links">
                          <h4>Our Social Networks</h4>
                          <p>Follow us on social media! See our design inspiration, development expertise, and happy
                              client stories! </p>
                          <div class="social-links mt-3">
                              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                          </div>
                      </div>

                  </div>
              </div>
          </div>
      </footer><!-- End Footer -->

      <!-- Vendor JS Files -->
      <script data-cfasync="false" src="{{asset('js/email-decode.min.js')}}"></script>
      <script src="{{asset('js/aos.js')}}"></script>
      <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
      <script src="{{asset('js/glightbox.min.js')}}"></script>
      <script src="{{asset('js/isotope.pkgd.min.js')}}"></script>
      <script src="{{asset('js/swiper-bundle.min.js')}}"></script>
      <script src="{{asset('js/noframework.waypoints.js')}}"></script>
      <script src="{{asset('js/validate.js')}}"></script>
      <script src="https://smtpjs.com/v3/smtp.js"></script>
      <script>
      function sendEmail() {
          Email.send({
              Host: "smtp.elasticemail.com",
              Username: "username",
              Password: "password",
              To: 'them@website.com',
              From: "you@isp.com",
              Subject: "This is the subject",
              Body: "And this is the body"
          }).then(
              message => alert(message)
          );
      }
      </script>

      <!-- Template Main JS File -->
      <script src="{{asset('js/main.js')}}"></script>
  </body>

  </html>